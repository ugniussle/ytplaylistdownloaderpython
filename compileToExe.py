import subprocess

compileUI = 'pyuic6 mainwindow.ui -o YTPlaylistDownloaderPython' + os.sep + 'MainWindow.py'
compileGetTitle = 'g++ -IC:\Libraries\curlmingw\include YTPlaylistDownloaderPython' + os.sep + 'getTitle.cpp -o getTitle.exe -LC:' + os.sep + 'Libraries' + os.sep + 'curlmingw' + os.sep + 'lib -lcurl -static-libgcc -static-libstdc++'
compilePy = 'pyinstaller --onefile YTPlaylistDownloaderPython' + os.sep + 'Main.py --upx-dir=..' + os.sep + '..' + os.sep + 'pythonenv' + os.sep + 'upx' + os.sep + ''
run = 'dist/Main.exe'

try:
    subprocess.run(compileUI.split(' '))
except:
    print('UI compilation failed')
    
try:
    subprocess.run(compileGetTitle.split(' '))
except:
    print('getTitle compilation failed')

try:
    subprocess.run(compilePy.split(' '))
except:
    print('program compilation failed')

try:
    subprocess.run(run.split(' '))
except:
    print('running failed')
