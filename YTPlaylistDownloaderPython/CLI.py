import YTPlaylistDownloader as dl

import Playlists, Presets, Tagger
from pprint import pprint


class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()
        super().__init__()

    def __call__(self): return self.impl()

class _GetchUnix:
    def __init__(self):
        super().__init__()

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            _ = tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        super().__init__()

    def __call__(self):
        import msvcrt
        return msvcrt.getch()

getch = _Getch()

def downloadPlaylists(entries: list[tuple[str, str, str]] | None = None):
    links, titles, presets = [], [], []
    if entries == None:
        playlists = Playlists.Playlists()

        for (link, title, preset) in zip(playlists.links, playlists.titles, playlists.presets):
            links.append(link)
            titles.append(title)
            presets.append(Presets.getPreset(preset))
            print(link, title, preset)
    else:
        links, titles, presets = entries[:0], entries[:1], entries[:2]

    playlistsData = []

    def permissionPrompt(file):
        print(f"the file {file} is open in a different program.")

        while True:
            char = getch()
            print("press \'r\' to retry, \'c\' to continue.")

            if char == b'r' or char == b'R':
                return True
            elif char == b'c' or char == b'C':
                return False
            else:
                continue

    for link, preset in zip(links, presets):
        data = dl.downloadPlaylist(
            preset =   preset,
            link =     link,
            progress = None,
            bar =      None
        )

        playlistsData.append(data)


    maxLoopIterations = len(playlistsData[0])

    print("_______________ playlists data _________________")
    pprint(playlistsData)

    for i, (videosInfo, oldVideosTags, preset) in enumerate(playlistsData):
        if maxLoopIterations <= i:
            print("max iterations break")
            break

        tag = Tagger.Tagger()
        for videoInfo in videosInfo:
            while(True):
                try:
                    tag.setTags(videoInfo)
                    break
                except:
                    if permissionPrompt(videoInfo['filepath']) == False:
                        break

        if preset.saveOldTags:
            for oldVideoTags in oldVideosTags:
                while(True):
                    try:
                        tag.setOldTags(oldVideoTags)
                        break
                    except:
                        if permissionPrompt(oldVideosTags['filepath']) == False:
                            break

        if preset.cutThumbnails:
            for videoInfo in videosInfo:
                while(True):
                    try:
                        tag.cutThumbnail(videoInfo, preset.saveOriginalThumbnail)
                        break
                    except:
                        if permissionPrompt(videoInfo['filepath']) == False:
                            break

    if len(playlistsData) == 0:
        print("Error: No videos were downloaded")
        exit(1)
    else:
        exit(0)
