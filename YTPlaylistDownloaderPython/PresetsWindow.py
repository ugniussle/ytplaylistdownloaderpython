from PyQt6.QtWidgets import QComboBox, QLineEdit, QCheckBox, QGroupBox
from Ui_PresetsWindow import Ui_PresetsWindow

import Presets

class PresetsWindow(QGroupBox, Ui_PresetsWindow):
    def __init__(self, *args, obj=None, **kwargs):
        super(PresetsWindow, self).__init__(*args,**kwargs)

        self.SelectedPreset: QComboBox
        self.SubtitlesCheck: QCheckBox
        self.Thumbnails: QCheckBox
        self.SubLang: QLineEdit
        self.SubFormat: QLineEdit
        self.Archive: QLineEdit
        self.Format: QLineEdit
        self.OutputTemplate: QLineEdit
        self.CutCovers: QCheckBox
        self.SaveOriginalCover: QCheckBox
        self.Chapters: QCheckBox
        self.OldTags: QCheckBox
        
        self.setupUi(self)

        self.SelectedPreset.currentTextChanged.connect(self.loadPreset)


    def loadPreset(self, presetName: str):
        preset = Presets.getPreset(presetName)

        self.SubtitlesCheck.setChecked(preset.subtitles)
        self.Thumbnails.setChecked(preset.thumbnails)
        self.CutCovers.setChecked(preset.cutThumbnails)
        self.SaveOriginalCover.setChecked(preset.saveOriginalThumbnail)
        self.Chapters.setChecked(preset.chapters)
        self.OldTags.setChecked(preset.saveOldTags)

        self.SubLang.setText(''.join(preset.subtitlesLanguage))
        self.SubFormat.setText(preset.subtitleFormat)
        self.Archive.setText(preset.archive)
        self.Format.setText(preset.format)
        self.OutputTemplate.setText(preset.outputTemplate)


