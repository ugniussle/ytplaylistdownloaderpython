import ffmpeg, mutagen, base64, filetype
from os import remove
from mutagen.flac import Picture

class Tagger:
    def checkIfFileOpen(self, filepath):
        try:
            file = mutagen.File(filepath)
            file.tags
            file.save()

            return 1

        except FileNotFoundError:
            return -1

        except (PermissionError, AttributeError, mutagen.MutagenError):
            return 0

    def setTags(self, videoInfo:dict):
        #print("opening mutagen file")
        #print(videoInfo['filepath'])
        file = mutagen.File(videoInfo['filepath'])

        file.tags['title'] = videoInfo.get('title')

        if videoInfo.get('album') != None:
            file.tags['album'] = videoInfo.get('album')

        if videoInfo.get('artist') != None:
            file.tags['artist'] = videoInfo.get('artist')

        if videoInfo.get('release_date') != None:
            date = videoInfo.get('release_date')
            file.tags['date'] = date[0:4]+'-'+date[4:6]+'-'+date[6:8]

        if videoInfo.get('playlist_index') != None:
            file.tags['tracknumber'] = str(videoInfo.get('playlist_index'))

        #print("saving tags")
        file.save()

    def cutThumbnail(self, videoInfo, saveOriginalThumbnail):
        try:
            remove('_cover.png')
        except FileNotFoundError:
            pass
        try:
            remove('cover.png')
        except FileNotFoundError:
            pass
        
        coverBack = self.extractCover(videoInfo['filepath'], 'cover.png')

        width, height = self.getPicDim(coverBack)

        if(width == height):
            remove('cover.png')
            return
        
        coverFront = self.cropImage(coverBack, width, height, '_cover.png')

        self.saveCoverToFile(coverFront, coverBack, videoInfo['filepath'], height, width, saveOriginalThumbnail)

        remove('_cover.png')
        remove('cover.png')

    def extractCover(self, file, output):
        stream = ffmpeg.input(file)
        stream = ffmpeg.output(stream, output, vframes=1, format='image2', vcodec='copy')
        stream = ffmpeg.overwrite_output(stream)
        try:
            ffmpeg.run(stream, capture_stdout=True, capture_stderr=True)
        except ffmpeg.Error as e:
            print(e.stderr.decode('utf-8'))

        return output

    def getPicDim(self, file):
        probe = ffmpeg.probe(file)
        video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
        width = int(video_stream['width'])
        height = int(video_stream['height'])

        return width, height

    def cropImage(self, file, width, height, output):
        stream = ffmpeg.input(file)
        stream = ffmpeg.crop(stream, (width - height) / 2, 0, height, height)
        stream = ffmpeg.output(stream, output)
        stream = ffmpeg.overwrite_output(stream)
        ffmpeg.run(stream, quiet=True)

        return output

    def saveCoverToFile(self, coverFront, coverBack, filename, height, width, saveOriginalThumbnail):
        file = mutagen.File(filename)
        kind = filetype.guess(filename)
        pic = Picture()
        pic.mime = kind.mime
        
        images = list()

        with open(coverFront,'rb') as thumbfile:
            pic.data = thumbfile.read()
        pic.type = 3
        pic.width  = height
        pic.height = height

        images.append(base64.b64encode(pic.write()).decode('ascii'))

        if(saveOriginalThumbnail == True):
            pic = Picture()
            kind = filetype.guess(coverBack)
            pic.mime = kind.mime
            with open(coverBack,'rb') as thumbfile:
                pic.data = thumbfile.read()
            pic.type = 4
            pic.width  = width
            pic.height = height

            images.append(base64.b64encode(pic.write()).decode('ascii'))

        file.tags['METADATA_BLOCK_PICTURE'] = images

        file.save()

    def extractTags(self, filepath):
        file = mutagen.File(filepath)

        return file.tags

    # make it so that old tags don't save if they are empty
    def setOldTags(self, oldVideoTags):
        try:
            file = mutagen.File(oldVideoTags['filepath'])
            file.tags = oldVideoTags['tags']
            file.save()
        except:
            print('setting old tags failed on', oldVideoTags['filepath'])
