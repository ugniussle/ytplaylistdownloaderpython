import sys
import CLI
from MainWindow import MainWindow
from PyQt6.QtWidgets import QApplication

def start():
    app = QApplication(sys.argv)

    window = MainWindow()

    window.show()
    _ = app.exec()

if len(sys.argv) > 1:
    print("This is broken, use UI instead")
    exit(1)

    if sys.argv[1] == '--all':
        CLI.downloadPlaylists()
        print("Finished downloading playlists")
        exit(0)

    entries: list[tuple[str, str, str]] = []
    i = 1
    while i < len(sys.argv):
        if sys.argv[i] == '-p':
            entries.append((sys.argv[i + 1], sys.argv[i + 2], sys.argv[i + 3]))
            i = i + 4
        else:
            print(
                """Usage:\n
                For specific playlists:\n
                -p <playlist link> <playlist name> <preset name>\n\n
                 For all playlists from playlists file:\n--all\n"""
            )

            exit()

    CLI.downloadPlaylists(entries)
else:
    start()
