//g++ -IC:\Libraries\curlmingw\include main.cpp -o main.exe -LC:\Libraries\curlmingw\lib -lcurl -static-libgcc -static-libstdc++

#include <curl/curl.h>
#include <iostream>
#include <string>

std::string getTitle(std::string url);
std::string getResponse(std::string);

int main(int argc, char** argv){
    std::cout<<getTitle(argv[1]);
    return 0;
}

size_t writeFunction(void* ptr, size_t size, size_t nmemb, std::string* data) {
    data->append((char*)ptr, size * nmemb);
    return size * nmemb;
}

std::string getResponse(std::string url) {
    auto curl = curl_easy_init();

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 50L);
        curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
        curl_easy_setopt(curl, CURLOPT_CAINFO, "./cacert.pem");
        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        std::string response_string;
        //std::string header_string;

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeFunction);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
        //curl_easy_setopt(curl, CURLOPT_HEADERDATA, &header_string);
        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        curl_global_cleanup();

        curl = NULL;

        return response_string;
    }

    return "";
}

size_t findTitleIndex(std::string str) {
    std::string to_find = "\"playlistMetadataRenderer\":{\"title\":\"";

    if(str.find(to_find,0)==std::string::npos) return 0;

    return str.find(to_find,0)+to_find.length();
}

std::string getTitle(std::string url) {
    std::string r = getResponse(url);
    size_t titleIndex = findTitleIndex(r);

    if (titleIndex == 0) {
        return "";
    }

    std::string title = "";

    for (size_t i = 0; r[i + titleIndex] != '\"'; i++) {
        title += r[titleIndex + i];
    }

    return title;
}
