class Preset:
    subtitles:bool
    subtitlesLanguage:str
    subtitleFormat:str
    archive:str
    thumbnails:bool
    cutThumbnails:bool
    saveOriginalThumbnail:bool
    format:str
    outputTemplate:str
    chapters:bool
    saveOldTags:bool
    extractAudio:bool
    outputFileType:str|None = None

audioPreset = Preset()

audioPreset.subtitles = True
audioPreset.subtitlesLanguage = ["en.*"]
audioPreset.subtitleFormat = "vtt"
audioPreset.archive = "audioArchive.txt"
audioPreset.thumbnails = True
audioPreset.cutThumbnails = True
audioPreset.saveOriginalThumbnail = True
audioPreset.format = "bestaudio"
audioPreset.outputTemplate = "%(playlist_title)s/%(title)s.%(ext)s"
audioPreset.chapters = True
audioPreset.saveOldTags = True
audioPreset.extractAudio = True

videoPreset = Preset()

videoPreset.subtitles = True
videoPreset.subtitlesLanguage = ["en.*"]
videoPreset.subtitleFormat = "vtt"
videoPreset.archive = "videoArchive.txt"
videoPreset.thumbnails = True
videoPreset.cutThumbnails = True
videoPreset.saveOriginalThumbnail = True
videoPreset.format = "bestvideo[height<=1080]+bestaudio/best[height<=1080]"
videoPreset.outputTemplate = "%(playlist_title)s/%(title)s.%(ext)s"
videoPreset.chapters = True
videoPreset.saveOldTags = False
videoPreset.extractAudio = False
videoPreset.outputFileType = "mp4"

def getPreset(preset:str):
    if preset == "Video":
        return videoPreset
    if preset == "Audio":
        return audioPreset
    # custom presets go here
    else:
        return Preset()
