# Form implementation generated from reading ui file 'presetswindow.ui'
#
# Created by: PyQt6 UI code generator 6.7.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic6 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt6 import QtCore, QtGui, QtWidgets


class Ui_PresetsWindow(object):
    def setupUi(self, PresetsWindow):
        PresetsWindow.setObjectName("PresetsWindow")
        PresetsWindow.resize(450, 450)
        PresetsWindow.setMinimumSize(QtCore.QSize(450, 450))
        PresetsWindow.setMaximumSize(QtCore.QSize(600, 502))
        self.gridLayout = QtWidgets.QGridLayout(PresetsWindow)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_5 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_2.addWidget(self.label_5)
        self.SelectedPreset = QtWidgets.QComboBox(parent=PresetsWindow)
        self.SelectedPreset.setMinimumSize(QtCore.QSize(200, 0))
        self.SelectedPreset.setObjectName("SelectedPreset")
        self.SelectedPreset.addItem("")
        self.SelectedPreset.addItem("")
        self.SelectedPreset.addItem("")
        self.verticalLayout_2.addWidget(self.SelectedPreset)
        self.SavePreset = QtWidgets.QPushButton(parent=PresetsWindow)
        self.SavePreset.setObjectName("SavePreset")
        self.verticalLayout_2.addWidget(self.SavePreset)
        self.label_9 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_9.setObjectName("label_9")
        self.verticalLayout_2.addWidget(self.label_9)
        self.SubtitlesCheck = QtWidgets.QCheckBox(parent=PresetsWindow)
        self.SubtitlesCheck.setChecked(True)
        self.SubtitlesCheck.setObjectName("SubtitlesCheck")
        self.verticalLayout_2.addWidget(self.SubtitlesCheck)
        self.Thumbnails = QtWidgets.QCheckBox(parent=PresetsWindow)
        self.Thumbnails.setToolTip("")
        self.Thumbnails.setChecked(True)
        self.Thumbnails.setObjectName("Thumbnails")
        self.verticalLayout_2.addWidget(self.Thumbnails)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.SubLang = QtWidgets.QLineEdit(parent=PresetsWindow)
        self.SubLang.setObjectName("SubLang")
        self.horizontalLayout_2.addWidget(self.SubLang)
        self.label = QtWidgets.QLabel(parent=PresetsWindow)
        self.label.setToolTip("")
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.SubFormat = QtWidgets.QLineEdit(parent=PresetsWindow)
        self.SubFormat.setObjectName("SubFormat")
        self.horizontalLayout_3.addWidget(self.SubFormat)
        self.label_2 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_2.setToolTip("")
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.Archive = QtWidgets.QLineEdit(parent=PresetsWindow)
        self.Archive.setObjectName("Archive")
        self.horizontalLayout_4.addWidget(self.Archive)
        self.label_3 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_3.setToolTip("")
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_4.addWidget(self.label_3)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.Format = QtWidgets.QLineEdit(parent=PresetsWindow)
        self.Format.setEnabled(True)
        self.Format.setObjectName("Format")
        self.horizontalLayout_5.addWidget(self.Format)
        self.label_4 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_4.setToolTip("")
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_5.addWidget(self.label_4)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)
        self.label_8 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_8.setOpenExternalLinks(True)
        self.label_8.setObjectName("label_8")
        self.verticalLayout_2.addWidget(self.label_8)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.OutputTemplate = QtWidgets.QLineEdit(parent=PresetsWindow)
        self.OutputTemplate.setObjectName("OutputTemplate")
        self.horizontalLayout_6.addWidget(self.OutputTemplate)
        self.label_6 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_6.setToolTip("")
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_6.addWidget(self.label_6)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.label_7 = QtWidgets.QLabel(parent=PresetsWindow)
        self.label_7.setOpenExternalLinks(True)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_2.addWidget(self.label_7)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.CutCovers = QtWidgets.QCheckBox(parent=PresetsWindow)
        self.CutCovers.setChecked(True)
        self.CutCovers.setObjectName("CutCovers")
        self.horizontalLayout_7.addWidget(self.CutCovers)
        self.SaveOriginalCover = QtWidgets.QCheckBox(parent=PresetsWindow)
        self.SaveOriginalCover.setMinimumSize(QtCore.QSize(190, 0))
        self.SaveOriginalCover.setChecked(True)
        self.SaveOriginalCover.setObjectName("SaveOriginalCover")
        self.horizontalLayout_7.addWidget(self.SaveOriginalCover)
        self.verticalLayout_2.addLayout(self.horizontalLayout_7)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.Chapters = QtWidgets.QCheckBox(parent=PresetsWindow)
        self.Chapters.setChecked(True)
        self.Chapters.setObjectName("Chapters")
        self.horizontalLayout_8.addWidget(self.Chapters)
        self.OldTags = QtWidgets.QCheckBox(parent=PresetsWindow)
        self.OldTags.setChecked(True)
        self.OldTags.setObjectName("OldTags")
        self.horizontalLayout_8.addWidget(self.OldTags)
        self.verticalLayout_2.addLayout(self.horizontalLayout_8)
        self.gridLayout.addLayout(self.verticalLayout_2, 0, 0, 1, 1)

        self.retranslateUi(PresetsWindow)
        QtCore.QMetaObject.connectSlotsByName(PresetsWindow)

    def retranslateUi(self, PresetsWindow):
        _translate = QtCore.QCoreApplication.translate
        PresetsWindow.setWindowTitle(_translate("PresetsWindow", "Presets"))
        self.label_5.setText(_translate("PresetsWindow", "Preset"))
        self.SelectedPreset.setItemText(0, _translate("PresetsWindow", "Audio"))
        self.SelectedPreset.setItemText(1, _translate("PresetsWindow", "Video"))
        self.SelectedPreset.setItemText(2, _translate("PresetsWindow", "Create New Preset..."))
        self.SavePreset.setText(_translate("PresetsWindow", "Save Preset"))
        self.label_9.setText(_translate("PresetsWindow", "Hover over text input for defaults."))
        self.SubtitlesCheck.setToolTip(_translate("PresetsWindow", "default: checked"))
        self.SubtitlesCheck.setText(_translate("PresetsWindow", "Write subtitles?"))
        self.Thumbnails.setText(_translate("PresetsWindow", "Write thumbnails?"))
        self.SubLang.setToolTip(_translate("PresetsWindow", "default: en   (separate more languages with comma and no space, example: en,ja)"))
        self.SubLang.setText(_translate("PresetsWindow", "en"))
        self.label.setText(_translate("PresetsWindow", "Subtitle languages"))
        self.SubFormat.setToolTip(_translate("PresetsWindow", "default: best"))
        self.SubFormat.setText(_translate("PresetsWindow", "lrc"))
        self.label_2.setText(_translate("PresetsWindow", "Subtitle format"))
        self.Archive.setToolTip(_translate("PresetsWindow", "default: archive.txt | downloaded videos get stored and will not be downloaded next time. Leave this field empty for no archive."))
        self.Archive.setText(_translate("PresetsWindow", "archive.txt"))
        self.label_3.setText(_translate("PresetsWindow", "Download archive"))
        self.Format.setToolTip(_translate("PresetsWindow", "default: bestaudio"))
        self.Format.setText(_translate("PresetsWindow", "bestaudio"))
        self.label_4.setText(_translate("PresetsWindow", "Format"))
        self.label_8.setText(_translate("PresetsWindow", "<a href=\"https://github.com/yt-dlp/yt-dlp#format-selection\">Format formatting</a>"))
        self.OutputTemplate.setToolTip(_translate("PresetsWindow", "default: %(playlist_title)s/%(title)s.%(ext)s"))
        self.OutputTemplate.setText(_translate("PresetsWindow", "%(playlist_title)s/%(title)s.%(ext)s"))
        self.label_6.setText(_translate("PresetsWindow", "Output template"))
        self.label_7.setText(_translate("PresetsWindow", "<a href=\"https://github.com/yt-dlp/yt-dlp#output-template\">Output template formatting</a>"))
        self.CutCovers.setText(_translate("PresetsWindow", "Cut covers to 1:1 aspect ratio?"))
        self.SaveOriginalCover.setText(_translate("PresetsWindow", "save uncut cover as back cover?"))
        self.Chapters.setText(_translate("PresetsWindow", "Save chapter data?"))
        self.OldTags.setText(_translate("PresetsWindow", "Do not overwrite old tags?"))
