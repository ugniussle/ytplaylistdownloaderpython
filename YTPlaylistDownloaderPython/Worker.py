from PyQt6.QtCore import *

class Worker(QRunnable):
    def __init__(self, function, progress=False, finished=False, *args, **kwargs):
        super(Worker, self).__init__()
        self.function = function
        self.args = args
        self.kwargs = kwargs

        self.signals = WorkerSignals()

        if progress == True:
            self.kwargs['progress_callback'] = self.signals.progress

        if finished == True:
            self.kwargs['finished_callback'] = self.signals.finished
    
    @pyqtSlot()
    def run(self):
        self.function(*self.args, **self.kwargs)

class WorkerSignals(QObject):
    progress = pyqtSignal(int)
    finished = pyqtSignal(object)

