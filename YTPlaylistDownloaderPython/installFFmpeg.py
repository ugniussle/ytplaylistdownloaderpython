import subprocess, requests, os, zipfile, shutil, ctypes

class FFmpegInstaller:
    def installFFmpeg(self):
        filename = self.downloadFFmpeg()

        with zipfile.ZipFile(filename) as z:
            extractedFolder = z.filelist[0].filename
            z.extractall()

        os.remove(filename)

        try:
            os.rename(extractedFolder, 'ffmpeg')
        except FileExistsError:
            shutil.rmtree('ffmpeg')

            os.rename(extractedFolder, 'ffmpeg')

        extractedFolder = 'ffmpeg'

        path = os.environ["PATH"] + ';' + os.getcwd() + os.sep + extractedFolder + os.sep + 'bin'

        ctypes.windll.shell32.ShellExecuteW(None, "runas", 'powershell',' [System.Environment]::SetEnvironmentVariable(\'Path\',\'' + path + '\', [System.EnvironmentVariableTarget]::Machine)', None, 1)

    def checkForFFMpeg(self):
        try:
            subprocess.Popen(['ffmpeg'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except FileNotFoundError:
            return False
        else:
            return True

    def downloadFFmpeg(self):
        link = 'https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-win64-lgpl-shared.zip'
        try:
            response = requests.get(link, stream=True)
        except:
            print('error: can\'t download file')
            exit()
        
        output = 'ffmpeg.'+link.split('.')[-1]

        with open(output, 'wb') as file:
            for chunk in response:
                file.write(chunk)
        
        return output