import subprocess, json

class Playlists:
    def __init__(self) -> None:
        super().__init__()
        self.links: list[str] = list()
        self.titles: list[str] = list()
        self.presets: list[str] = list()

        self.readPlaylists()
        
    @staticmethod
    def getTitle(url: str):
        try:
            result = subprocess.run(['getTitle.exe ', url], stdout=subprocess.PIPE)

            title = str(result.stdout).split('\'')[1]
            title = title.replace("\\n", "")
            title = title.replace("\\r", "")
        except Exception as e:
            print(e)

            title = ""

        return title

    def readPlaylists(self):
        try:
            file = open("playlists.txt","r")
        except FileNotFoundError:
            self.savePlaylists()
            file = open("playlists.txt","r")

        playlists: dict[str, list[dict[str, str]]] | None
        try:
            playlists = json.load(file)
        except json.JSONDecodeError:
            playlists = None

        if type(playlists) == dict:
            for playlist in playlists["playlists"]:
                self.links.append(playlist["link"])
                self.titles.append(playlist["title"])
                self.presets.append(playlist["preset"])

        file.close()

    def savePlaylists(self):
        file = open("playlists.txt","w")

        playlists: dict[str, list[dict[str, str]]] = dict()

        playlists["playlists"] = list()

        for i in range(len(self.links)):
            if self.links[i] != "":
                playlist = {
                    "link": self.links[i],
                    "title": self.titles[i],
                    "preset": self.presets[i] 
                }

                playlists["playlists"].append(playlist)

        #print(playlists)

        json.dump(playlists, file, indent=4)
        
        file.close()
