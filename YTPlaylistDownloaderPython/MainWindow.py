from PyQt6.QtWidgets import *
from PyQt6.QtCore import *

import YTPlaylistDownloader as dl

from Ui_MainWindow import Ui_MainWindow
from PresetsWindow import PresetsWindow

from Dialog import Dialog
from installFFmpeg import FFmpegInstaller
from Worker import Worker
from Tagger import Tagger
import Presets
import Playlists

import traceback


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, obj=None, **kwargs):
        super(MainWindow, self).__init__(*args,**kwargs)

        self.PlaylistTable: QTableWidget
        self.SavePlaylists: QPushButton
        self.AddPlaylist: QPushButton
        self.SetPlaylistNames: QPushButton
        self.DownloadButton: QPushButton
        self.OpenPresetsEditor: QAction

        self.threadpool = QThreadPool()

        self.busy = False
        
        self.setupUi(self)

        self.importPlaylists()

        #set preset row width
        self.PlaylistTable.setColumnWidth(2, 120)

        self.SavePlaylists.clicked.connect(self.savePlaylists)
        self.AddPlaylist.clicked.connect(self.addPlaylist)
        self.SetPlaylistNames.clicked.connect(self.setPlaylistNamesDriver)
        self.DownloadButton.clicked.connect(self.downloadPlaylistsDriver)

        self.OpenPresetsEditor.triggered.connect(self.openPresetsWindow)

        FFmpegDL = FFmpegInstaller()

        if FFmpegDL.checkForFFMpeg() == 0:
            dlg = Dialog('FFmpeg needed', 'FFmpeg not found. It is needed for this program to function. Install FFmpeg?', 2)
            if dlg.exec() == 1:
                FFmpegDL.installFFmpeg()

        self.progressBar.setFormat('Waiting...')

    def openPresetsWindow(self):
        self.presetsWindow = PresetsWindow()

        self.presetsWindow.show()


    def finishedDownload(self, data:object):
        self.tagVideos(data)

    def downloadPlaylistsDriver(self):
        if(self.busy == True):
            self.dialog('Error', 'Different process is under way. Please wait for it to finish.', 1)
            return

        self.busy = True

        worker = Worker(
            function = self.downloadPlaylists,
            progress = True,
            finished = True
        )
        worker.signals.progress.connect(self.progressFunction)
        worker.signals.finished.connect(self.finishedDownload)

        self.threadpool.start(worker)

    def downloadPlaylists(self, progress_callback: pyqtBoundSignal, finished_callback: pyqtBoundSignal):
        links, _, presets = self.getPlaylistsInfo(selected=True)

        videosData = []

        self.progressBar.setFormat("Downloading %v / %m")

        for link, preset in zip(links, presets):
            data = dl.downloadPlaylist(
                link =      link,
                preset =    preset,
                progress =  progress_callback,
                bar =       self.progressBar
            )

            videosData.append(data)

        self.busy = False

        finished_callback.emit(videosData)

    def dialog(self, title, msg, buttons, buttonText = None):
        dlg = Dialog(title, msg, buttons, buttonText)

        return dlg.exec()

    def permissionDialog(self, file):
        return self.dialog(
            title='Permission error',
            msg='The file {} is open in another program. Please close all programs that could be using the new files and press \'Retry\''.format(file),
            buttons=2,
            buttonText=['Retry', 'Skip']
        )

    def tagVideos(self, playlistsData):
        tag = Tagger()

        preset:Presets.Preset

        maxLoopIterations = len(playlistsData[0])
        self.progressBar.setMaximum(maxLoopIterations)
        self.progressBar.setValue(0)

        for i, (videosInfo, oldVideosTags, preset) in enumerate(playlistsData):
            if maxLoopIterations <= i:
                break

            for videoInfo in videosInfo:
                while(True):
                    try:
                        print("setting tags on " + videoInfo['filepath'])
                        tag.setTags(videoInfo)
                        break
                    except Exception as e:
                        traceback.print_exception(e)
                        if self.permissionDialog(videoInfo['filepath']) == 0:
                            break

            if preset.saveOldTags:
                for oldVideoTags in oldVideosTags:
                    while(True):
                        try:
                            #print("setting old tags")
                            tag.setOldTags(oldVideoTags)
                            break
                        except Exception as e:
                            traceback.print_exception(e)
                            if self.permissionDialog(videoInfo['filepath']) == 0:
                                break

            if preset.cutThumbnails:
                for videoInfo in videosInfo:
                    while(True):
                        try:
                            #print("cutting thumbnails")
                            tag.cutThumbnail(videoInfo, preset.saveOriginalThumbnail)
                            break
                        except Exception as e:
                            traceback.print_exception(e)
                            if self.permissionDialog(videoInfo['filepath']) == 0:
                                break

            i = i + 1
            self.progressFunction(i)

        self.progressBar.setFormat("Downloading and tagging completed.")
        self.progressBar.setMaximum(1)
        self.progressBar.setValue(1)

    def progressFunction(self, value):
        self.progressBar.setValue(value)

    def setPlaylistNamesDriver(self):
        if(self.busy == True):
            self.dialog('Error', 'Different process is under way. Please wait for it to finish.', 1)
            return
        
        self.busy = True

        self.progressBar.setFormat("Setting titles %v / %m")

        worker = Worker(self.setPlaylistNames, True)
        worker.signals.progress.connect(self.progressFunction)

        self.progressBar.setRange(0, self.PlaylistTable.rowCount())

        self.threadpool.start(worker)

    def setPlaylistNames(self, progress_callback = None):
        links, titles, _ = self.getPlaylistsInfo()
        for i in range(0, len(links)):
            try:
                progress_callback.emit(i)
            except:
                pass

            if links[i] == '':
                continue

            if titles[i] == '':
                titles[i] = Playlists.Playlists.getTitle(links[i])

                self.PlaylistTable.item(i,1).setData(0, titles[i])

            try:
                progress_callback.emit(i)
            except:
                pass

        self.progressBar.setFormat("Setting titles completed.")

        self.busy = False

    def addPlaylist(self):
        count = self.PlaylistTable.rowCount()
        self.PlaylistTable.insertRow(count)

        linkitem = QTableWidgetItem()
        titleItem = QTableWidgetItem()
        presetItem = QComboBox()
        presetItem.addItem("Audio")
        presetItem.addItem("Video")

        self.PlaylistTable.setItem(count,0,linkitem)
        self.PlaylistTable.setItem(count,1,titleItem)
        self.PlaylistTable.setCellWidget(count,2,presetItem)

    def importPlaylists(self):
        P = Playlists.Playlists()

        self.PlaylistTable.setRowCount(len(P.links))

        for i in range(0,len(P.links)):
            item = QTableWidgetItem(P.links[i])

            self.PlaylistTable.setItem(i,0,item)
            try:
                if P.titles[i] != None:
                    item = QTableWidgetItem(P.titles[i])

                    self.PlaylistTable.setItem(i,1,item)
            except TypeError:
                pass

            try:
                if P.presets[i] != None:
                    item = QComboBox()
                    item.addItem("Audio")
                    item.addItem("Video")

                    if P.presets[i] == "Video":
                        item.setCurrentIndex(1)
                    else:
                        item.setCurrentIndex(0)

                    self.PlaylistTable.setCellWidget(i,2,item)
            except TypeError:
                pass

    def savePlaylists(self):
        links, titles, presets = self.getPlaylistsInfo()
        
        P = Playlists.Playlists()

        P.links = links
        P.titles = titles
        P.presets = presets

        P.savePlaylists()

    def getPlaylistsInfo(self,selected=False):
        links = list()
        titles = list()
        presets = list()

        # for saving
        if not selected:
            for i in range(0, self.PlaylistTable.rowCount()+1):
                try:
                    links.append( self.PlaylistTable.item(i,0).text().strip() )
                except AttributeError:
                    continue
                try:
                    titles.append( self.PlaylistTable.item(i,1).text().strip() )
                except AttributeError:
                    titles.append('')
                try:
                    #print(type(self.PlaylistTable.cellWidget(i, 2)))
                    preset:QComboBox = self.PlaylistTable.cellWidget(i, 2)
                    
                    presets.append( preset.itemText(preset.currentIndex()) )
                except AttributeError:
                    presets.append('')
        # for downloading in UI
        else:
            items = self.PlaylistTable.selectedItems()

            for item in items:
                if item.column() == 0:
                    try:
                        text:str = item.text().strip()
                    except AttributeError:
                        text:str = ""

                    links.append(text)

                    presetBox = self.PlaylistTable.cellWidget(item.row(), 2)
                    presetName = presetBox.itemText(presetBox.currentIndex())

                    #print("preset name", presetName)

                    presets.append(Presets.getPreset(presetName))

        #print(links, titles, presets)

        return links, titles, presets
