import yt_dlp
import json
import os
import copy
import Tagger
from Presets import *
from pprint import pprint


def jsonFilter(json_object: dict[str, str]):
    keysTokeep = ['title', 'playlist_index', 'album',
                  'artist', 'release_date', 'entries', 'filepath', 'id']
    jsonKeys = list(json_object.keys())
    for key in jsonKeys:
        if not (key in keysTokeep):
            _ = json_object.pop(key)
    return json_object


videosInfo = list()
saveChapters = False


def postHook(info: dict):
    if info['status'] == 'finished' and info['postprocessor'] == 'MoveFiles':
        if saveChapters == True and type(info['info_dict']['chapters']) != 'dict' and info['info_dict']['chapters'] != None: 
            writeChapters(info['info_dict']['chapters'],
                          info['info_dict']['filepath'])

        videosInfo.append(jsonFilter(info['info_dict']))


def writeChapters(chapters, filename: str):
    chaptersFilename = filename[0:filename.rfind('.')]+'.chapters'
    file = open(chaptersFilename, 'wt')
    _ = file.write(json.dumps(chapters))
    file.close()


progress_callback = None
progressBar = None
prevFilesAndTags = None


def progressHook(info: dict):
    if info['status'] == 'finished':
        try:
            progressBar.setRange(0, int(info['info_dict']['playlist_count']))
        except:
            pass
        try:
            progress_callback.emit(int(info['info_dict']['playlist_index']))
        except:
            pass

    if info['status'] == 'downloading' and prevFilesAndTags != None and info['info_dict']['ext'] != 'vtt':
        filepath:str = copy.copy(info['info_dict']['_filename'])

        dir = filepath[0:filepath.find(os.sep)]
        filename = filepath[filepath.find(os.sep) + 1:filepath.rfind('.')]

        if len(prevFilesAndTags) > 0:
            lastPrevFilePath = prevFilesAndTags[len(prevFilesAndTags) - 1]['filepath']

            if (filename == lastPrevFilePath[lastPrevFilePath.find(os.sep) + 1:lastPrevFilePath.rfind('.')]):
                return

        prevFilepath = findPreviousFile(filename, dir)

        if prevFilepath != None:
            tag = Tagger.Tagger()
            tags = tag.extractTags(prevFilepath)

            prevFilesAndTags.append({'filepath': prevFilepath, 'tags': tags})

def findPreviousFile(newFilename: str, dir: str):
    allowedExts = ['opus', 'mp4', 'mp3']

    for file in os.listdir(dir):
        ext = file[file.rfind('.')+1:len(file)]
        filename = file[0:file.rfind('.')]

        if ext in allowedExts and filename == newFilename:
            return dir+os.sep+file

    return None

def downloadPlaylist(link: str, preset: Preset, progress, bar):
    global progressBar
    progressBar = bar

    global progress_callback
    progress_callback = progress

    global saveChapters
    saveChapters = preset.chapters

    global prevFilesAndTags

    if preset.saveOldTags == True:
        prevFilesAndTags = list()
    else:
        prevFilesAndTags = None

    ydl_opts = {
        'quiet': False,
        'writesubtitles': preset.subtitles,
        'subtitleslangs': preset.subtitlesLanguage,
        'subtitlesformat': preset.subtitleFormat,
        'writethumbnail': preset.thumbnails,
        'format': preset.format,
        'ignoreerrors': True,
        'postprocessors': [],
        'postprocessor_hooks': [postHook],
        'progress_hooks': [progressHook],
        'outtmpl': {'default': preset.outputTemplate}
    }

    if preset.extractAudio:
        ydl_opts['postprocessors'].append(
            {
                'key': 'FFmpegExtractAudio',
                'preferredquality': 0
            }
        )

    if preset.thumbnails:
        ydl_opts['postprocessors'].append(
            {
                'key': 'EmbedThumbnail'
            }
        )

    if preset.subtitles:
        ydl_opts['postprocessors'].append(
            {
                'key': 'FFmpegSubtitlesConvertor',
                'format': 'lrc'
            }
        )

    if preset.outputFileType != None:
        ydl_opts['merge_output_format'] = preset.outputFileType

    if preset.archive != '':
        ydl_opts['download_archive'] = preset.archive

    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        ydl.extract_info(link)

    return [videosInfo, prevFilesAndTags, preset]
