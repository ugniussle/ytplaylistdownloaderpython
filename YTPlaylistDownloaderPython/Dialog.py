from PyQt6.QtWidgets import *

class Dialog(QDialog):
    def __init__(self, title, msg, buttons=2, buttonText=['Yes', 'No']):
        super().__init__()

        self.setWindowTitle(title)

        self.layout = QVBoxLayout()
        
        msgObj = QLabel()
        msgObj.setText(msg)

        self.layout.addWidget(msgObj)

        if buttons > 0:
            self.buttonBox = QDialogButtonBox()

            if buttons == 2:
                self.buttonBox.addButton(buttonText[0], QDialogButtonBox.ButtonRole.AcceptRole)
                self.buttonBox.addButton(buttonText[1], QDialogButtonBox.ButtonRole.RejectRole)

            elif buttons == 1:
                button:QDialogButtonBox.StandardButton = QDialogButtonBox.Ok
                self.buttonBox.setStandardButtons(button)
            
            self.buttonBox.accepted.connect(self.accept)
            self.buttonBox.rejected.connect(self.reject)

            self.layout.addWidget(self.buttonBox)

        self.setLayout(self.layout)
