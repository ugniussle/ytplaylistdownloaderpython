# YTPlaylistDownloaderPython

Python Qt5 tool to download and tag youtube playlists. Currently only works for audio.

![example](ex.png)

## Instructions

1. Press 'Add new playlist' button.
2. Paste a full link to a playlist into 'Playlist URL' column of the newly created row.
3. (optional) Either type in the name of the playlist to see it on next launch or press 'Get playlist names automatically'.
   1. On first automatic playlist name getting you need to accept Youtube cookies in the created window.
   2. Press the Close button on the Cookies window.
4. Press 'Save playlists' to save playlists for next launch.
5. Select which playlists you want to download (select multiple with 'Shift' key).
6. Press the big 'Download' button.

## How do I remove playlists?

Leave both fields as empty.

## Compiling to an executable

``` python compileToExe.py ```

## Running in python

``` python run.py ```

## Required Python modules

1. PyQt5
2. yt_dlp
3. mutagen
4. ffmpeg-python
