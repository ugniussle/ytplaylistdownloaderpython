import subprocess

compileMainWindow = 'pyuic6 mainwindow.ui -o YTPlaylistDownloaderPython/Ui_MainWindow.py'
compilePresetsWindow = 'pyuic6 presetswindow.ui -o YTPlaylistDownloaderPython/Ui_PresetsWindow.py'
# fix compilation
# compileGetTitle = 'g++ -IC:\Libraries\curlmingw\include YTPlaylistDownloaderPython/getTitle.cpp -o getTitle.exe -LC:\Libraries\curlmingw\lib -lcurl -static-libgcc -static-libstdc++'
run = 'python YTPlaylistDownloaderPython/Main.py'

try:
    subprocess.run(compileMainWindow.split(' '))
except:
    print('Main window compilation failed')

try:
    subprocess.run(compilePresetsWindow.split(' '))
except:
    print('Presets window compilation failed')

# try:
#     subprocess.run(compileGetTitle.split(' '))
# except:
#     print('getTitle compilation failed')

try:
    subprocess.run(run.split(' '))
except:
    print('running failed')
